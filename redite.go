package redite

import (
	"encoding/json"
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/my0sot1s/helper"
)

// RedisCli rd
type RedisCli struct {
	client   *redis.Client
	duration time.Duration
}

// InitRd start rd
func (rc *RedisCli) InitRd(rdHost, rdPW string) error {
	rc.client = redis.NewClient(&redis.Options{
		Addr:     rdHost,
		Password: rdPW, // no password set
		DB:       0,    // use default DB
	})
	rc.duration = 60 * 10 * time.Second // 10 phut
	_, err := rc.client.Ping().Result()
	return err
}

// SetValue to rd
func (rc *RedisCli) SetValue(key string, value string, expiration time.Duration) error {
	return rc.client.Set(key, value, expiration).Err()
}

// GetValue to rd
func (rc *RedisCli) GetValue(key string) (string, error) {
	val, err := rc.client.Get(key).Result()
	return val, err
}

// DelKey hard del key
func (rc *RedisCli) DelKey(key []string) (int, error) {
	val, err := rc.client.Del(key...).Result()
	return int(val), err
}

// LPushItem push multiple
func (rc *RedisCli) LPushItem(key string, timeExpired int, values ...interface{}) error {
	for _, v := range values {
		b, err := json.Marshal(v)
		if _, err = rc.client.LPush(key, string(b)).Result(); err != nil {
			return err
		}
	}
	rc.SetExpired(key, timeExpired)
	return nil
}

// LRangeAll get multiple
func (rc *RedisCli) LRangeAll(key string) ([]map[string]interface{}, error) {
	var raw []string
	raw, err := rc.client.LRange(key, 0, -1).Result()
	if err != nil {
		return nil, err
	}
	resp := make([]helper.M, 0)
	for _, v := range raw {
		var d helper.M
		json.Unmarshal([]byte(v), &d)
		resp = append(resp, d)
	}
	return resp, err
}

// SetExpired set expried
func (rc *RedisCli) SetExpired(key string, min int) bool {
	d := time.Duration(min) * time.Minute
	b, _ := rc.client.Expire(key, d).Result()
	return b
}
